import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Le site de démo en classe Jeudi';
  //let d = new Date();
  datedujour= new Date().getFullYear();
  lien1="assets/images/web.png";
  lienimg="assets/images/Pavillon_Judith-Jasmin_UQAM_11.JPG";
}
